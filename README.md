# Spring Boot Redis Implementation
Redis is driven by a keystore-based data structure to persist data and can be used as a database, cache, message broker, etc. This code is an introduction to Spring Data Redis, which provides the abstractions of the Spring Data platform to Redis.

## Maven dependencies used
```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<dependency>
	<groupId>redis.clients</groupId>
	<artifactId>jedis</artifactId>
</dependency>
```

## Redis Download Location
[https://redis.io/download](https://redis.io/download)

## Code Structure
1. Redis Configuration using Jedis
2. User Model Class
3. Rest Controller
4. Service Class
5. Crud Repository

## Rest APIs
- GET request to http://localhost:8080/users to get all users
- GET request to http://localhost:8080/user/{id} to get specific user
- POST request to http://localhost:8080/user to add a user
- PUT request to http://localhost:8080/user to edit any user

```json
{
	"id" : 1,
	"firstName" : "John",
	"lastName" : "Doe",
	"emailId" : "john@domain.com",
	"age" : 35
}
```

### Build the Project skipping Tests
```mvn
mvn clean install -DskipTests
```

### Run the Project
```mvn
java -jar spring-boot-data-redis-0.0.1-SNAPSHOT.jar
```

