package com.pratyush.redis.model;

import java.io.Serializable;

import org.springframework.data.redis.core.RedisHash;

import lombok.Data;

@Data
@RedisHash("User")
@SuppressWarnings("serial")
public class User implements Serializable {	
	private Long id;
	private String firstName;	
	private String lastName;
	private String emailId;
	private int age;
	
}