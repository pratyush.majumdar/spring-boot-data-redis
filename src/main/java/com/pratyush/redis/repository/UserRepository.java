package com.pratyush.redis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pratyush.redis.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
}